import json
import os
import random

import requests
from flask import Flask, send_file
from flask import request
from flask_cors import CORS
from redis import Redis

# Connect to Redis
from werkzeug.utils import secure_filename

redis = Redis(host='redis', port=6379, db=0)

app = Flask(__name__)
CORS(app)


def send(target_node, nodes_that_have_file, filepath):
    """send a filepath to chosen node so that it replicates that file to other nodes"""
    # from where to replicate
    try:
        source_node = nodes_that_have_file[random.randint(0, len(nodes_that_have_file))]
    except:
        source_node = nodes_that_have_file[0]

    data = {'node': target_node, 'filepath': filepath}
    print(f"Sending REPLICATE file '{filepath}' from {source_node} to {target_node}")
    r = requests.post("http://" + source_node.decode("UTF-8") + "/replicate", data=data)

    if r.status_code != 200:
        print(
            f"REPLICATION failed on file {filepath} from {source_node} to {target_node} with code {r.status_code}: {r.reason}")

    return r.status_code, r.reason


def synchronize(node):
    """Send all needed files to the new connected node"""

    # get dict of all files in system {filepath: [node1_address, node2_address]}
    files = {}
    folders = []
    for filepath in redis.smembers('files'):
        if filepath and not filepath.decode("UTF-8")[-1] == '/':
            files[filepath] = list(redis.smembers(filepath))
        elif filepath and filepath.decode("UTF-8")[-1] == '/':
            folders.append(filepath.decode("UTF-8"))

    # send files that are NOT already on the node from the node that has them
    for filepath, nodes_that_have_file in files.items():
        if node not in nodes_that_have_file:
            status_code, reason = send(node, nodes_that_have_file, filepath)

    # send folders to the new node
    for folder in folders:
        data = {'nodes': json.dumps([]), 'filepath': folder}

        r = requests.post("http://" + node.decode("UTF-8") + "/mkdir", data=data)
        if r.status_code != 200:
            print(f"REPLICATE folder {folder} failed with code {r.status_code}: {r.reason}")

    # remove node from new nodes (because it is synchronized now)
    redis.srem("new_nodes", node)


def health_check(nodes):
    """send ping to all of datanodes"""
    healthy_nodes = []
    for i in nodes:
        try:
            url = "http://" + i.decode("UTF-8") + "/healthcheck"
            r = requests.post(url)
            if r.status_code == 200:
                healthy_nodes.append(i.decode("UTF-8"))
        except:
            # if node is not responding, remove it from list of nodes
            redis.srem('nodes', i.decode("UTF-8"))
            continue

    # if there are new nodes in the system, synchronize them.
    new_nodes_synced_count = 0
    for new_node in redis.smembers('new_nodes'):
        if new_node.decode('UTF-8') in healthy_nodes:
            synchronize(new_node)
            new_nodes_synced_count += 1

    print(f"HEALTH CHECK results: "
          f"\n           {len(healthy_nodes)} nodes are available;"
          f"\n           {len(nodes) - len(healthy_nodes)} nodes deleted;"
          f"\n           {new_nodes_synced_count} new nodes synced")

    return healthy_nodes


@app.route("/send", methods=["POST"])
def upload():
    """
    POST: create a file
    :body_param: file (example: "file.png")
    :body_param: filepath - path where file should be saved (example: "folder1/folder2/")
    This example will be saved as "folder1/folder2/file.png"
    """
    # get the file and save it on local storage
    if 'file' not in request.files:
        return app.make_response(({'error': 'No file found'}, 404))
    file = request.files['file']

    if file.filename == '':
        return app.make_response(({'error': 'No file found'}, 404))
    if file:
        filename = secure_filename(file.filename)
        filepath = request.form['filepath']
        path = "./files/" + filepath
        if not os.path.isdir(path):
            os.makedirs(path)
        file.save(os.path.join(path, filename))

    # get healthy nodes
    all_nodes = [i for i in redis.smembers("nodes")]
    nodes = health_check(all_nodes)

    # send the file (and other node IPs) to node 1
    files = {'upload_file': open(path + filename, 'rb')}
    data = {'nodes': json.dumps(nodes[1:] if len(nodes) > 1 else []), 'filepath': filepath}

    print(f"Sending CREATE file '{filepath + filename}' to {nodes}")
    r = requests.post("http://" + nodes[0] + "/upload", files=files, data=data)

    # remove from local storage
    os.remove(path + filename)

    if r.status_code == 200:
        # if successfully added, store path on redis

        redis.sadd("files", filepath + filename)
        for i in nodes:
            redis.sadd(filepath + filename, i)

            if not redis.exists(filepath):
                for node in nodes:
                    redis.sadd(filepath, node)
            redis.sadd('files', filepath)

        return app.make_response(("success", 200))
    else:
        return app.make_response((r.reason, r.status_code))


@app.route("/mkdir", methods=["POST"])
def mkdir():
    """
    :body_param filepath - path where the folder should be.
    """
    data = json.loads(request.data)
    filepath = os.path.join(data['filepath'], "")

    filepath = filepath.replace("//", "/")
    if filepath[0] == "/":
        filepath = filepath[1:]

    path = "./files/" + filepath

    if filepath in redis.smembers('files'):
        return app.make_response(("File already exists", 304))

    # get healthy nodes
    all_nodes = [i for i in redis.smembers("nodes")]
    nodes = health_check(all_nodes)

    # send to node 1 the file and other node IPs
    data = {'nodes': json.dumps(nodes[1:] if len(nodes) > 1 else []), 'filepath': filepath}

    print(f"Sending MKDIR '{str(filepath)}' to {nodes}")
    r = requests.post("http://" + nodes[0] + "/mkdir", data=data)

    if r.status_code == 200:
        # if successful, add the folder path to redis
        redis.sadd('files', filepath)
        for i in nodes:
            redis.sadd(filepath, i)
        return app.make_response(("success", 200))
    else:
        return app.make_response((r.reason, r.status_code))


@app.route("/touch", methods=["POST"])
def touch():
    """
    POST: create an empty file
    :body_param: file (example: "file.png")
    :body_param: filepath - path where file should be saved (example: "folder1/folder2/")
    This example will be saved as "folder1/folder2/file.png"
    """
    data = json.loads(request.data)
    filename = data['filename']
    filepath = data['filepath']

    # to send a file, in python you need to create it first
    with open("./files/" + filename, 'w') as file:
        file.write("")

    path = "./files/" + filepath

    # get healthy nodes to to send files to
    all_nodes = [i for i in redis.smembers("nodes")]
    nodes = health_check(all_nodes)

    # send to node 1 the file and other node IPs
    files = {'upload_file': open("./files/" + filename, 'rb')}
    data = {'nodes': json.dumps(nodes[1:] if len(nodes) > 1 else []), 'filepath': filepath}

    print(f"Sending TOUCH file '{str(filepath) + str(filename)}' to {nodes}")
    r = requests.post("http://" + nodes[0] + "/upload", files=files, data=data)

    os.remove("./files/" + filename)

    if r.status_code == 200:
        # if successful, store filepath in redis
        for i in nodes:
            redis.sadd(filepath + filename, i)
            redis.sadd("files", filepath + filename)
        return app.make_response(("success", 200))
    else:
        return app.make_response((r.reason, r.status_code))


@app.route("/move", methods=["POST"])
def move():
    """
    POST: move a file from one place to another
    :body_param: source - from where to move file
    :body_param: target - to where to move file
    """
    data = json.loads(request.data)
    source = data['source']
    target = data['target']

    # get healthy nodes
    nodes = redis.smembers(source)
    nodes = health_check(nodes)

    if not nodes:
        # it is an empty set on redis (thus no nodes have that file) -> file doesn't exist
        return app.make_response(("File does not exist", 400))

    print(f"Sending MOVE from '{source}' to '{target}' to {nodes}")
    data = {"nodes": json.dumps(nodes[1:]), "source": source, "target": target}
    r = requests.post("http://" + nodes[0] + "/move", data=data)

    if r.status_code == 200:
        # if successful, remove the old filepath from redis..
        redis.srem("files", source)
        redis.sadd("files", target)

        members = redis.smembers(source)
        redis.delete(source)

        # ..and store the new filepath
        if not redis.exists(target):
            path = ''
            for i in target.split('/')[:-1]:
                path = path + i + '/'
                redis.sadd('files', path)
                for node in members:
                    redis.sadd(path, node)
        redis.sadd('files', target)
        for node in members:
            redis.sadd(target, node)
        return app.make_response(("success", 200))
    else:
        return app.make_response((r.reason, r.status_code))


@app.route("/remove", methods=["POST"])
def remove():
    """
    POST: remove a file from the system
    body_param: filepath - the full path to the file (example: "/folder1/folder2/img.png")
    """
    data = json.loads(request.data)
    filepath = data['filepath']
    confirmed = data.get('confirmed')  # if a folder and contains files, we need explicit confirmation of deletion

    nodes = [i for i in redis.smembers(filepath)]

    if not nodes:
        nodes = [i for i in redis.smembers(filepath + "/")]

    if not nodes:
        return app.make_response(("File does not exist", 404))

    # get healthy nodes
    nodes = health_check(nodes)

    data = {"filepath": filepath}
    info = requests.get("http://" + nodes[0] + "/info", params=data)

    info = info.json()

    # if filepath is a non-empty directory, ask for confirmation
    if info['file_type'] == 'directory' and not info.get('is_empty') and not confirmed:
        return app.make_response(("Need confirmation", 304))

    print(f"Sending REMOVE file '{filepath}' to {nodes}")

    data = {"nodes": json.dumps(nodes[1:]), "filepath": filepath, "is_folder": info['file_type'] == 'directory'}
    r = requests.post("http://" + nodes[0] + "/remove", data=data)

    if r.status_code == 200:
        # if successful, remove the filepath from redis
        if not info['file_type'] == 'directory':
            redis.srem("files", filepath)
            redis.delete(filepath)
        else:
            redis.srem("files", filepath if filepath[-1] == '/' else filepath + "/")
            redis.delete(filepath if filepath[-1] == '/' else filepath + "/")
            # if directory, delete all files in it
            for i in redis.smembers('files'):
                i = i.decode("UTF-8")
                if i.startswith(filepath if filepath[-1] == '/' else filepath + '/'):
                    redis.srem('files', i)
                    redis.delete(i)
        return app.make_response(("success", 200))
    else:
        return app.make_response((r.reason, r.status_code))


@app.route("/copy", methods=["POST"])
def copy():
    """
    POST: copies file from one place to another
    :body_param: source - from where to copy file
    :body_param: target - to where to copy file
    """
    data = json.loads(request.data)
    source = data['source']
    target = data['target']

    nodes = redis.smembers(source)
    nodes = health_check(nodes)
    if not nodes:
        return app.make_response(("File does not exist", 400))

    print(f"Sending COPY from '{source}' to '{target}' to {nodes}")
    data = {"nodes": json.dumps(nodes[1:]), "source": source, "target": target}
    r = requests.post("http://" + nodes[0] + "/copy", data=data)

    if r.status_code == 200:
        # if successful, add the new filepath to redis (no deletion of old info like in move())
        if not redis.exists(target):
            path = ''
            for i in target.split('/')[:-1]:
                path = path + i + '/'
                redis.sadd('files', path)
                for node in nodes:
                    redis.sadd(path, node)
        redis.sadd('files', target)
        for node in nodes:
            redis.sadd(target, node)

        return app.make_response(("success", 200))
    else:
        return app.make_response((r.reason, r.status_code))


@app.route("/get", methods=["GET"])
def get_file():
    """
    query_param: filepath - full path to file
    :return: the file
    """
    filename = request.args.get("filepath")

    # nodes who have the file
    nodes = redis.smembers(filename)

    # checking if they are alive
    nodes = health_check(nodes)

    # choosing random node from alive nodes
    try:
        node = nodes[random.randint(0, len(nodes) - 1)]
    except:
        # if there is only one node (somehow random.randint() cannot select random number in range (0, 0))
        node = nodes[0]

    print(f"Sending GET file '{filename}' from {node}")

    r = requests.get("http://" + node + "/retrieve", params={"filepath": filename})

    if r.status_code != 200:
        app.make_response((r.reason, r.status_code))
    path = "./files/" + filename

    # to return the file, we need to store it (and then delete at the end)
    with open(f"./files/{filename.split('/')[-1]}", 'wb') as f:
        for chunk in r:
            f.write(chunk)

    # sending file as a response
    res = send_file(f"./files/{filename.split('/')[-1]}")

    # delete the file from local storage
    os.remove(f"./files/{filename.split('/')[-1]}")
    return res


@app.route("/info", methods=["GET"])
def get_file_info():
    """
    GET info of the file
    query_param: filepath - full path to file
    :return: info of the file in JSON
    """
    filename = request.args.get("filepath")

    nodes = redis.smembers(filename)
    nodes = health_check(nodes)

    if not nodes:
        # try with ending slash (maybe it's a folder)
        nodes = [i.decode("UTF-8") for i in redis.smembers(filename + "/")]

    if not nodes:
        print(f"Couldn't retrieve info about {filename}")
        return app.make_response((f"File {filename} not found", 404))

    try:
        node = nodes[random.randint(0, len(nodes) - 1)]
    except:
        # if only one node in the system
        node = nodes[0]

    print(f"Sending INFO with file '{filename}' from {node}")

    r = requests.get("http://" + node + "/info", params={"filepath": filename})

    if r.status_code == 200:
        info = r.json()
        return app.make_response((json.dumps({"info": info}), 200))
    else:
        return app.make_response((r.reason, r.status_code))


@app.route("/get_all", methods=["GET"])
def get_all():
    """
    GET all file paths (mostly for debug purposes)
    :return: ['folder1/folder2/img.png', 'folder3/file.doc', 'img1.jpg']
    """
    files = [i.decode("UTF-8") for i in redis.smembers("files")]
    return app.make_response((json.dumps(files), 200))


@app.route("/ls", methods=["GET"])
def ls():
    """
    GET current directory and all the files and directories in it.
    query_param: directory (example: "dir1/dir2/")
    :return: {"files": ['file1.png', 'file2.jpg'], "folders": ['dir1', 'dir2']}
    """
    dir = request.args.get('directory')

    files = [i.decode("UTF-8") for i in redis.smembers("files")]

    files_in_dir = set()
    dirs_in_dir = set()
    for i in files:
        # search for filepaths which start with our folder path
        if i.startswith(dir if dir == "" else (dir if dir[-1] == '/' else dir + '/')):
            obj = i[len(dir):]
            # filter out the files which are in subdirectories (we don't need them at this level)
            if "/" in obj and not obj.split("/")[0] == "":
                dirs_in_dir.add(obj.split("/")[0])
            elif "/" not in obj and not obj.split("/")[0] == "":
                files_in_dir.add(obj.split("/")[0])

    data = {"directories": list(dirs_in_dir), "files": list(files_in_dir)}

    return app.make_response((json.dumps(data), 200))


@app.route("/join", methods=["POST"])
def add_node():
    """
    Endpoint for nodes to join the system (not used by client)

    :body_param address - the IP address and port of the new node
    :body_param name - name of the node (optional and currently not used)
    """

    try:
        address = request.form.get('address') + ':5010'
    except:
        return app.make_response((json.dumps(request), 200))
    nodes = [i.decode("utf-8") for i in redis.smembers("nodes")]
    if address in nodes:
        # if node already exists in redis, do not add it
        return app.make_response(("Already in system", 304))

    # add the node to redis
    redis.sadd("nodes", address)
    print(redis.smembers('nodes'))

    # if it is not the only node in the system, then we need to synchronize files in them, so we put it in new_nodes
    if len(redis.smembers('nodes')) > 1:
        redis.sadd('new_nodes', address)
    return app.make_response(("success", 200))


@app.route("/stats", methods=["GET"])
def get_all_node_stats():
    """
    Get info about the nodes currently in the system
    """
    nodes = [i for i in redis.smembers("nodes")]
    nodes = health_check(nodes)

    mem_per_node = {}
    for node in nodes:
        r = requests.get("http://" + node + "/stats")

        if r.status_code == 200:
            mem_per_node[node] = {"available_storage": float(r.json()['free_mem']),
                                  "full_storage": float(r.json()['mem'])}

    return app.make_response((json.dumps({"mem_per_node": mem_per_node}), 200))


@app.route("/ping")
def ping():
    return app.make_response((json.dumps({"status": "success"}), 200))


@app.route("/f", methods=["DELETE"])
def flush():
    """
    Force all datanodes to delete their data. Clear redis in the namenode Those who responded are included in the new list of nodes.
    """
    nodes_new = []
    available_storage = 0
    full_storage = 0
    old_node_count = len(redis.smembers('nodes'))
    for node in redis.smembers('nodes'):
        node = node.decode("UTF-8")
        r = requests.delete("http://" + node + "/f")

        if r.status_code == 200:
            nodes_new.append(node)
            available_storage += float(r.json()['free_mem'])
            full_storage += float(r.json()['mem'])

    redis_status = redis.flushall()
    for node in nodes_new:
        redis.sadd('nodes', node)

    print(f"FLUSH results:"
          f"\n     Namenode cleared: {bool(redis_status)};"
          f"\n     Datanodes responded: {len(nodes_new)}/{old_node_count}"
          f"\n     Available storage: {available_storage}/{full_storage} GB")

    return app.make_response(("success", 200))


if __name__ == "__main__":
    if not os.path.exists("./files"):
        os.mkdir("./files")
    app.run(host='0.0.0.0', port=8000)
