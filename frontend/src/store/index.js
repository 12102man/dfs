import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import _ from 'lodash'
Vue.use(Vuex)

const HOST_ADDR = 'http://3.133.121.92:8000'

const api = axios.create({
    baseURL: HOST_ADDR
});

export default new Vuex.Store({
    state: {
        path: '',
        uploadFile: false,
        newFile: false,
        newFolder: false,
        nodesList: false,
        nodesLoading: false,
        nodes: [],
        fileInfo: {},
        fileInfoShow: false,
        moveFile: false,
        copyShow: false,
        deleteShow: false,
        filename: "",
        files: {
            directories: [],
            files: []
        }
    },
    mutations: {
        set_path(state, path) {
            state.path += path
        },
        close_all(state) {
            state.newFile = false;
            state.nodesList = false;
            state.newFolder = false;
            state.uploadFile = false;
            state.moveFile = false;
            state.copyShow = false;

        },
    },
    actions: {
        setFile({state}, nm) {
            state.filename = nm
        },
        setPath({commit, dispatch, state}, path) {
            commit('set_path', path)
            dispatch('getFiles')
        },

        goBack({dispatch, state}) {
            console.log(state.path.split('/'))
            state.path = state.path.slice(0, -1).split('/').slice(0, -1).join('/') + '/'
            if (state.path === '/') {
                state.path = ''
            }
            dispatch('getFiles')
        },
        getFiles({commit, state, dispatch}) {
            return api.get('/ls', {
                params: {
                    directory: state.path
                }
            }).then(resp => {
                state.files = resp.data;
                commit('close_all');
                dispatch('getNodes')
            })
        },
        downloadFile({state}, filename) {
            const path = state.path + filename
            api.get('/get', {
                params: {
                    filepath: path
                },
                responseType: 'blob'
            }).then((response) => {
                const url = window.URL.createObjectURL(new Blob([response.data]));
                const link = document.createElement('a');
                link.href = url;
                link.setAttribute('download', filename);
                document.body.appendChild(link);
                link.click();
            });
        },
        switchUploadWindow({state}) {
            state.uploadFile = !state.uploadFile
        },
        switchNewFile({state}) {
            state.newFile = !state.newFile
        },
        switchNewFolder({state}) {
            state.newFolder = !state.newFolder
        },
        switchFileInfo({state}) {
            state.fileInfoShow = !state.fileInfoShow
        },
        switchDelete({state}) {
            state.deleteShow = !state.deleteShow
        },
        switchNodesList({state}) {
            state.nodesList = !state.nodesList
        },
        switchMove({state}) {
            state.moveFile = !state.moveFile
        },
        switchCopy({state}) {
            state.copyShow = !state.copyShow
        },
        flush({dispatch}) {
            api.delete('/f').then(() => dispatch('getFiles'))
        },
        uploadFilesToServer({state, dispatch}, files) {
            for (let file of files) {
                var formdata = new FormData()
                formdata.append('file', file)
                formdata.append('filepath', state.path)
                api.post('/send', formdata).then(() => dispatch('getFiles'))
            }
        },
        createFile({state, dispatch}, filename) {
            api.post('/touch', {
                filename: filename,
                filepath: state.path
            }).then(() => dispatch('getFiles'))
        },
        move({state, dispatch}, filepath) {
            api.post('/move', {
                source: getURL(state.path, state.filename),
                target: getURL(filepath, state.filename)
            }).then(() => dispatch('getFiles'))
        },
        copy({state, dispatch}, filepath) {
            api.post('/copy', {
                source: getURL(state.path, state.filename),
                target: getURL(filepath, state.filename)
            }).then(() => dispatch('getFiles'))
        },
        createFolder({state, dispatch}, filename) {
            api.post('/mkdir', {
                filepath: getURL(state.path, filename)
            }).then(() => dispatch('getFiles'))
        },
        getNodes({state}) {
            state.nodesLoading = true
            api.get('/stats').then(resp => {
                state.nodes = resp.data.mem_per_node;
                state.nodesLoading = false
            })
        },
        getFileInfo({state}, filename) {
            api.get('/info', {
                params: {
                    filepath: getURL(state.path, filename)
                }
            }).then(resp => state.fileInfo = resp.data.info)
        },


        delete({state, dispatch}) {
            api.post('/remove', {
                    filepath: getURL(state.path, state.filename)
            }).then(() => dispatch('getFiles')).catch(e => {
                if (e.response.status === 304) {
                    state.deleteShow = true
                }
            })
        },
        deleteConfirmed({state, dispatch}) {
            api.post('/remove', {
                filepath: getURL(state.path, state.filename),
                confirmed: true
            }).then(() => {
                state.deleteShow = false
                dispatch('getFiles')
            })
        }
    },
    getters: {
        nodesNumber: state => _.keys(state.nodes).length === 0 ? "-" : _.keys(state.nodes).length
    }
})


function getURL(path, filename) {
    if (!!!path) {
        return filename
    } else {
        return (path + '/' + filename).replace("//", '/')
    }
}
