import json
import os
import shutil
import socket
import time

import requests

from flask import Flask, request, send_file
from werkzeug.utils import secure_filename

app = Flask(__name__)


base_path = "./files/"


@app.route('/healthcheck', methods=['POST'])
def healthcheck():
    return app.make_response(({}, 200))


@app.route('/copy', methods=['POST'])
def copy():
    filepath = request.form['source']
    ext_filepath = os.path.join(base_path, filepath)
    copypath = request.form['target']
    ext_copypath = os.path.join(base_path, copypath)
    nodes = json.loads(request.form['nodes'])

    if not os.path.isfile(ext_filepath):
        return app.make_response(({'error': 'File not found'}, 404))

    path_wo_filename = ""
    for elem in ext_copypath.split('/')[:-1]:
        path_wo_filename += str(elem) + "/"

    if not os.path.exists(path_wo_filename):
        os.makedirs(path_wo_filename)
    shutil.copyfile(ext_filepath, ext_copypath)
    if len(nodes) > 0:
        data = {'nodes': json.dumps(nodes[1:]), 'source': request.form['source'], 'target': request.form['target']}
        r = requests.post("http://" + nodes[0] + "/copy", data=data)
        if r.status_code == 200:
            return app.make_response(("success", 200))
        else:
            return app.make_response(("something went wrong", 500))
    return app.make_response(("success", 200))


@app.route('/move', methods=['POST'])
def move():
    filepath = request.form['source']

    ext_filepath = os.path.join(base_path, filepath)
    movepath = request.form['target']
    ext_copypath = os.path.join(base_path, movepath)

    nodes = json.loads(request.form['nodes'])

    if not os.path.isfile(ext_filepath):
        return app.make_response(({'error': 'File not found'}, 404))

    path_wo_filename = ""
    for elem in ext_copypath.split('/')[:-1]:
        path_wo_filename += str(elem) + "/"

    if not os.path.exists(path_wo_filename):
        os.makedirs(path_wo_filename)

    shutil.copyfile(ext_filepath, ext_copypath)
    os.remove(ext_filepath)
    if len(nodes) > 0:
        data = {'nodes': json.dumps(nodes[1:]), 'source': request.form['source'], 'target': request.form['target']}
        r = requests.post("http://" + nodes[0] + "/move", data=data)
        if r.status_code == 200:
            return app.make_response(("success", 200))
        else:
            return app.make_response(("something went wrong", 500))
    return app.make_response(("success", 200))


@app.route('/remove', methods=['POST'])
def remove():
    filepath = os.path.join(base_path, request.form['filepath'])
    nodes = json.loads(request.form['nodes'])

    if os.path.isdir(filepath):
        shutil.rmtree(filepath)
    else:
        os.remove(filepath)

    if len(nodes) > 0:
        data = {'nodes': json.dumps(nodes[1:]), 'filepath': request.form['filepath']}
        r = requests.post("http://" + nodes[0] + "/remove", data=data)
        if r.status_code == 200:
            return app.make_response(("success", 200))
        else:
            return app.make_response(("something went wrong", 500))
    return app.make_response(("success", 200))


@app.route('/upload', methods=['POST'])
def upload_file():
    if 'upload_file' not in request.files:
        return app.make_response(({'error': 'No file found'}, 404))
    file = request.files['upload_file']

    if file.filename == '':
        return app.make_response(({'error': 'No file found, filename govno'}, 404))
    if file:
        filename = secure_filename(file.filename)

        if not 'filepath' in request.form:
            filepath = ''
        else:
            filepath = request.form['filepath']

        extended_filepath = os.path.join(base_path, filepath)

        if not os.path.isdir(extended_filepath):
            os.makedirs(extended_filepath)

        file.save(os.path.join(extended_filepath, filename))
        nodes = json.loads(request.form['nodes'])
        files = {'upload_file': open(os.path.join(extended_filepath, filename), 'rb')}

        if len(nodes) > 0:
            data = {'nodes': json.dumps(nodes[1:]), 'filepath': filepath}
            r = requests.post("http://" + nodes[0] + "/upload", files=files, data=data)
            print(r.status_code)
            if r.status_code == 200:
                return app.make_response(("success", 200))
            else:
                return app.make_response(("something went wrong", 500))
        return app.make_response(({'detail': 'Vse pizdato'}, 200))
    return app.make_response(({'error': 'Trouble occured'}, 403))


@app.route('/replicate', methods=['POST'])
def replicate():
    filepath = request.form['filepath']
    extended_filepath = os.path.join(base_path, filepath)

    filename = filepath.split('/')[-1]

    if not os.path.exists(extended_filepath):
        return app.make_response(({'error': 'No file found, filename govno'}, 404))

    node = request.form['node']
    files = {'upload_file': open(os.path.join(extended_filepath), 'rb')}

    data = {'nodes': json.dumps([]), 'filepath': filepath.replace(filename, "")}

    r = requests.post("http://" + node + "/upload", files=files, data=data)
    print(r.status_code)
    if r.status_code == 200:
        return app.make_response(("success", 200))
    else:
        return app.make_response(("something went wrong", 500))


@app.route('/mkdir', methods=['POST'])
def create_directory():
    directory = request.form['filepath']
    directory = directory if directory[0] != 0 else directory[1:]
    extended = os.path.join(base_path, directory)
    if not os.path.isdir(extended):
        print(extended)
        os.makedirs(extended)
    else:
        return app.make_response(('Already exists', 304))

    nodes = json.loads(request.form['nodes'])
    print(nodes)

    if len(nodes) > 0:
        data = {'nodes': json.dumps(nodes[1:]), 'filepath': directory}
        r = requests.post("http://" + nodes[0] + "/mkdir", data=data)
        print(r.status_code)
        if r.status_code == 200:
            return app.make_response(("success", 200))
        else:
            return app.make_response(("something went wrong", 500))
    return app.make_response(({'detail': 'Vse pizdato'}, 200))


@app.route('/retrieve')
def retrieve():
    filename = os.path.join(base_path, request.args['filepath'])
    if os.path.isdir(filename):
        return app.make_response(("Is a directory", 400))
    return send_file(filename)


@app.route('/info', methods=['GET'])
def info():
    filepath = request.args.get('filepath')
    extended_filepath = os.path.join(base_path, filepath)
    if not os.path.exists(extended_filepath):
        return app.make_response(({'error': 'File not found'}, 404))
    elif os.path.isdir(extended_filepath):
        file_type = 'directory'
    else:
        file_type = 'file'

    filedata = os.stat(os.path.join(base_path, filepath))
    file_size = filedata.st_size / 1024
    recent_file_change = filedata.st_mtime

    if file_type == 'directory':
        files = False
        if len(os.listdir(extended_filepath)) == 0:
            files = True
        data = {'filepath': filepath, 'file_type': file_type, 'file_size': file_size,
                'recent_change': recent_file_change, 'is_empty': files}
    else:
        data = {'filepath': filepath, 'file_type': file_type, 'file_size': file_size,
                'recent_change': recent_file_change}
    return app.make_response((json.dumps(data), 200))


@app.route('/f', methods=['DELETE'])
def flush():
    if os.path.exists("./files"):
        shutil.rmtree(base_path)
    total, _, free_size = shutil.disk_usage('./')
    free_size /= 2 ** 30
    total /= 2 ** 30
    return app.make_response((json.dumps({'mem': format(total, '.1f'), 'free_mem': format(free_size, '.1f')}), 200))


@app.route('/stats', methods=['GET'])
def statistics():
    total, _, free_size = shutil.disk_usage('./')
    free_size /= 2 ** 30
    total /= 2 ** 30
    return app.make_response((json.dumps({'mem': format(total, '.1f'), 'free_mem': format(free_size, '.1f')}), 200))

def get_Host_name_IP():
    while True:
        try:
            host_name = socket.gethostname()
            host_ip = socket.gethostbyname(host_name)
            print("Hostname :  ",host_name)
            print("IP : ",host_ip)
            return host_ip
        except:
            print("Unable to get Hostname and IP")
            time.sleep(5)

def connect():

    while True:
        try:
            r = requests.post('http://namenode:8000/join', data={'address': get_Host_name_IP()})
            print(r.status_code)
            if r.status_code == 304:
                print('Fathernode already knows about me')
                return True
            elif r.status_code == 404:
                print('Father not responded good')
                time.sleep(5)
            elif r.status_code == 500:
                print('Internal jopa occured')
                time.sleep(5)
            else:
                if not os.path.exists(base_path):
                    os.mkdir(base_path)
                print('Now I have father')
                return True
        except requests.exceptions.ConnectionError:
            print('Father not found')
            time.sleep(5)


if __name__ == '__main__':
    connect()
    if not os.path.exists("./files"):
        os.mkdir("./files")
    app.run(host="0.0.0.0", port=5010)
