# Distributed File System


# How to launch
Please make sure that your host has Docker and Docker Compose installed.


Install Docker: https://www.digitalocean.com/community/tutorials/docker-ubuntu-18-04-1-ru

Install Docker Compose: https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-16-04

All you need to do is to clone this repository and do
`docker-compose -f docker-compose.dev.yml up --build`


NOTE!
Frontend needs to have an ip of namenode. Change HOST_ADDR in frontend/store/index.js to the address of namenode

# Architecture

![](https://i.imgur.com/KdSSXnK.png)

Namenode machine consists of 3 containers:
-   Frontend app - client communicates only with it
-   Namenode server - a RESTful Flask server which handles all operations
-   Redis filesystem - it is the core of namenode - the storage for all paths of files and folders and nodes in the system

Datanode machine consists of only the Datanode server

# How it works:
**1. Datanodes join**

![](https://i.imgur.com/9zhmhp9.png)

Datanodes send a /join request to the Namenode server, it resolves the IP of the Datanode, puts it in nodes list, and synchronizes files with it if needed.

**2. Redis storage architecture**

![](https://i.imgur.com/OsenGD7.png)

There are 4 main blocks in Redis:

1.  Nodes. All nodes in the system are stored here. They are deleted if at some point they become unreachable.
    
2.  New nodes. When a new (and not the first) node is connected to the system, it is added to ‘nodes’, but also to ‘new_nodes’. It indicates that this node’s files need to be synchronized. Once the synchronization has been done, the node is deleted from new_nodes list.
    
3.  Files. This is where all the filepaths and folderpaths lie. When a new file is uploaded, ‘files’ is appended by the filepath.
    
4.  Filepaths-nodes. When a new file is uploaded, Redis creates an entry with filepath as a key, and list of nodes that contain that file as a value.


**3. Working with files. Let’s take uploading a new file. Most of the other operations behave in a similar manner.**

1.  Client uploads the file through Frontend application.
    
2.  Frontend sends POST /upload to Namenode with the file in the request body
    
3.  Health check. The Namenode sends ‘ping’ to all of the nodes in the system. The file will be uploaded only to those nodes which respond. 

![](https://i.imgur.com/zJL9FhZ.png)

4.  Namenode picks randomly a Datanode from the list of healthy nodes and sends it POST /upload with the new file and list of nodes, which will receive the replica of the file. Waits for the response
    
5.  The Datanode receives the file, saves it in local storage and sends POST /upload with the file to the next node, and waits for a response
    
6.  When all replicas are saved, the first Datanode returns 200 to the Namenode
    
7.  Namenode saves the location of the file, and list of nodes where it is stored, to Redis storage.

![](https://i.imgur.com/HEHefyB.png)

# Communication protocols
All communication between nodes and client is done with REST API

